var request = require('request');
var async = require('async');

/**
 * Uses CoreLogic APIs to get properties based on search criteria
 */
module.exports.properties = function(rent, frequency, postcode, likedProperties) {
    return new Promise(function(resolve, reject) {
        var monthlyRent = calcMonthlyRent(rent, frequency);
        var rate = parseFloat(0.035 / 12);
        var nbrPeriods = 25 * 12;
        var pv = Math.floor(monthlyRent * ((1 - ( 1 / Math.pow((1 + rate), nbrPeriods))) / rate));
        var low = Math.floor(pv / 100000) * 100000;
        var high = low + 100000;        
    /*
        Pmt	 $1,603.33
        Rate Per Period	0.002916667
        Number of Periods	 300
        (1 + rate) ^ Nbr Periods	 2.395822113
        1 / (1 + rate) ^ Nbr Periods	 0.42
        1 - (1 / (1 + rate) ^ Nbr Periods)	 0.58
        (1 - (1 / (1 + rate) ^ Nbr Periods)) / rate	 199.75
        Pmt * ((1 - (1 / (1 + rate) ^ Nbr Periods)) / rate)	 $320,267.25
    */
        var results = {
            rent: rent,
            pv: pv,
            low: low,
            high: high,
            postcode: postcode,
            weekly: (frequency == "weekly"),
            monthly: (frequency == "monthly"),
            fortnightly: (frequency == "fortnightly")
        }

        getAccessToken()
        .then(function(accessToken) {
            return getSuggestedpostcodeId(postcode, accessToken)
        })
        .then(function(obj) {
            if (obj.postcodeId == "") {
                results.noresults = true;
                return resolve(results);
            }
            return getProperties(low + '-' + high, obj.postcodeId, obj.accessToken, likedProperties);
        })
        .then(function(obj) {
            results.properties = obj.properties;
            return resolve(results);
        })
    })
}

module.exports.comparableProperties = function(propertyId) {
    return new Promise(function(resolve, reject) {
        getAccessToken()
        .then(function(accessToken) {
            getComparableProperties(propertyId, accessToken)
                .then(function(results) {
                    return (properties)
                });
        })
        .then(function(properties) {
                results.properties = properties;
                return resolve(results);
            });
        })
}

/**
 * Gets access token from core logic
 * @returns {Promise (accessToken)}
 */
function getAccessToken() {
    return new Promise(function (resolve, reject) {
        var clientId = "77038e23";
        var clientSecret = "4e20ce98d903ae72c05baeff72d8cbfd"
        var uri = "https://access-api.corelogic.asia/access/oauth/token?client_id=" + clientId + "&client_secret=" + clientSecret + "&grant_type=client_credentials"
        console.log(uri);
        request(uri, function(err, response, body) {
            if (err) {
                return reject(err);
            }
            var results = JSON.parse(response.body);
            return resolve(results.access_token);
        })
    });
}

function getSuggestedpostcodeId(postcode, accessToken) {
    return new Promise(function (resolve, reject) {
        var uri = "https://property-sandbox-api.corelogic.asia/bsg-au/v1/suggest.json?q=" + postcode + "&access_token=" + accessToken;
        console.log(uri);
        request(uri, function(err, response, body) {
            if (err) {
                return reject(err);
            }
            var results = JSON.parse(response.body);
            var postcodeId = "";
            if (results.suggestions.length > 0) {
                postcodeId = results.suggestions[0].postcodeId;
            }
            return resolve({ postcodeId: postcodeId, accessToken: accessToken });
        })
    })
}

/**
 * Calls core logic to get a list of properties
 * @param price
 * @param accessToken
 * @returns {Promise}
 */
function getProperties(price, postcodeId, accessToken, likedProperties) {
    return new Promise(function (resolve, reject) {
        var uri = "https://search-sandbox-api.corelogic.asia/search/au/property/postcode/" + postcodeId + "/otmForSale?price=" + price + "&size=5&access_token=" + accessToken;
        console.log(uri);
        request(uri, function(err, response, body) {
            if (err) {
                return reject(err);
            }
            var results = JSON.parse(response.body);
            console.log(results);

            // parse results
            var properties = [ ];
            var counter = 0;
            results._embedded.propertySummaryList.map(function(p) {
                var propertySummary = p.propertySummary;
                console.log(propertySummary);
                if (counter < 5) {
                    var attributes = [ ];
                    if (propertySummary.attributes.bathrooms) { attributes.push("Bathrooms " + propertySummary.attributes.bathrooms); }
                    if (propertySummary.attributes.bedrooms) { attributes.push("Bedrooms " + propertySummary.attributes.bedrooms); }
                    if (propertySummary.attributes.carSpaces) { attributes.push("Car spaces " + propertySummary.attributes.carSpaces); }
                    if (propertySummary.attributes.landArea) { attributes.push("Land area " + propertySummary.attributes.landArea); }
                    if (propertySummary.attributes.lockUpGarages) { attributes.push("Lock Up Garages " + propertySummary.attributes.lockUpGarages); }

                    properties.push({
                        "id": propertySummary.id,
                        "address": propertySummary.address.singleLineAddress,
                        "agency": propertySummary.otmForSaleDetail.agency,
                        "largePhotoUrl": propertySummary.propertyPhoto.largePhotoUrl,
                        "mediumPhotoUrl": propertySummary.propertyPhoto.mediumPhotoUrl,
                        "thumbnailPhotoUrl": propertySummary.propertyPhoto.thumbnailPhotoUrl,
                        "priceDescription": propertySummary.otmForSaleDetail.priceDescription || "",
                        "attributes": attributes.join(", "),
                        "liked": (likedProperties && likedProperties[propertySummary.id])
                    });
                }
 
                counter++;
            });
            return resolve({ properties: properties, accessToken: accessToken });
        });
    });
}

function getComparableProperties(propertyId, accessToken) {
    var uri = 'https://property-sandbox-api.corelogic.asia/bsg-au/v1/property/comparables.json?access_token=' + accessToken;
    console.log('getComparablePropeties:uri', uri);
    var body = {
        "propertyId":propertyId,
        "comparablesRuleId": 2,
        "returnDetailForComparableCategoryId" : ["1", "2","3","4"],
        "returnFields" : ["address","attributes","propertyPhotoList"],
        "targetPropertyValuation": 400000,
        "limit": 10
    }

    return new Promise(function(resolve, reject) {
        request({
            url: uri,
            method: 'POST',
            json: body
        }, function(err, response, body) {
            if (err) {
                return reject(err);
            }
            var results = response.body;
            var comps = [ ];
            results.comparablesSummaryList.map(function(compSummary){
                var comparableCategory = compSummary.comparableCategory;
                if (compSummary.propertyComparableList) {
                    compSummary.propertyComparableList.map(function(property) {
                        var comp = { }
                        comp.comparableCategory = comparableCategory;

                        if (property.comparableForSalePropertyCampaign) {

                            if (property.comparableForSalePropertyCampaign.agency) {
                                if (property.comparableForSalePropertyCampaign.agency.company) {
                                    comp.companyName = property.comparableForSalePropertyCampaign.agency.company.companyName || '';
                                }
                            }
                            comp.priceDescription = property.comparableForSalePropertyCampaign.priceDescription || '';
                        }
                        if (property.property) {
                            comp.id = property.property.id;
                            if (property.property.address) {
                                comp.address = property.property.address.singleLine || '';
                            }
                            if (property.property.attributes) {
                                var attributes = [ ];
                                if (property.property.attributes.bathrooms) attributes.push('Bathrooms ' + property.property.attributes.bathrooms);
                                if (property.property.attributes.bedrooms) attributes.push('Bedrooms ' + property.property.attributes.bedrooms);
                                if (property.property.attributes.carSpaces) attributes.push('Car Spaces ' + property.property.attributes.carSpaces);
                                if (property.property.attributes.floorArea) attributes.push('Floor Area ' + property.property.attributes.floorArea);
                                if (property.property.attributes.lockUpGarages) attributes.push('Lock up garages ' + property.property.attributes.lockUpGarages);

                                comp.attributes = attributes.join(', ');
                            }
                            if (property.property.propertyPhotoList) {
                                property.property.propertyPhotoList.map(function(photo) {
                                    if (photo.isDefaultPhoto) {
                                        comp.photo = photo;
                                    }
                                })
                            }
                        }
                        comps.push(comp);

                    })
                }


            });
            return resolve({ properties: comps, accessToken: accessToken });
        });
    });
}

function calcMonthlyRent(rent, frequency) {
    if (frequency == "weekly") {
        return (rent * 52) / 12;
    } else if (frequency == "fortnightly") {
        return (rent * 26) /12;
    } else {
        return rent;
    }
}
