var crypto = require('crypto');

/**
 * Creates a Profile for a customer
 * @param email
 * @param password
 * @param profileData
 */
module.exports.createProfile = function (email, password, profileData) {
    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://localhost:27017/custportaldb";

    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var passwordSalt = generateSalt(16);
            var passwordHash = hashPassword(password, passwordSalt);
            var myobj = {
                email,
                passwordHash,
                passwordSalt,
                profileData
            };
            db.collection("customers").insertOne(myobj, function (err, res) {
                if (err) {
                    reject(err);
                }
                db.close();

                resolve(res);
            });
        });
    });
}

module.exports.authenticateUser = function (email, password) {
    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://localhost:27017/custportaldb";

    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var query = {
                email: email
            };
            db.collection("customers").findOne(query, function (err, result) {
                if (err) {
                    return reject(err);
                }
                var authenticatedUser = {}
                if (!result) {
                    authenticatedUser.authenticated = false;
                    authenticatedUser.reason = "User not found";
                    return resolve(authenticatedUser);
                }
                var passwordHash = hashPassword(password, result.passwordSalt);
                if (!passwordHash === result.passwordHash) {
                    authenticatedUser.authenticated = false;
                    authenticatedUser.reason = "Incorrect password";
                    return resolve(authenticatedUser);
                }
                authenticatedUser.authenticated = true;
                authenticatedUser.email = email;
                authenticatedUser.userId = result._id;
                authenticatedUser.profileData = result.profileData;

                return resolve(authenticatedUser);
            });
        });
    })
}

function getCustomer(email) {
    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://localhost:27017/custportaldb";
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var query = {
                email: email
            };
            db.collection("customers").findOne(query, function (err, result) {
                if (err) {
                    return reject(err);
                }
                console.log(result);
                var customer = {}
                if (result) {
                    customer.email = result.email;
                    customer.profileData = result.profileData;
                }
                return resolve(customer);
            });
        });
    })
}
module.exports.getCustomer = getCustomer;

module.exports.likeProperty = function (email, propertyId) {

    getCustomer(email)
        .then(function (customer) {
            return customer;
        })
        .then(function (customer) {
            var MongoClient = require('mongodb').MongoClient;
            var url = "mongodb://127.0.0.1:27017/custportaldb";

            if (!customer.profileData) {
                customer.profileData = {};
            }
            if (!customer.profileData.likedProperties) {
                customer.profileData.likedProperties = {};
            }
            customer.profileData.likedProperties[propertyId] = 'Liked';
            return new Promise(function (resolve, reject) {
                MongoClient.connect(url, function (err, db) {
                    if (err) throw err;
                    var myquery = {
                        email: email
                    };
                    var newvalues = {
                        email: customer.email,
                        passwordHash: customer.passwordHash,
                        passwordSalt: customer.passwordSalt,
                        profileData: customer.profileData
                    };
                    db.collection("customers").updateOne(myquery, newvalues, function (err, res) {
                        if (err) { return reject(err) };
                        db.close();
                        return resolve(customer.profileData.likedProperties);
                    });
                });
            });
        });
}

generateSalt = function (length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length); /** return required number of characters */
};

hashPassword = function (password, salt) {
    var hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    return hash.digest('hex');
};