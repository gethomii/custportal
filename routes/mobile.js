var express = require('express');
var searchOrchestration = require('../orchestrations/search');
var userOrchestration = require('../orchestrations/users');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    var vm = {
        title: 'Mobile - Get Homii',
        authenticated: (req.session.email) ? true : false,
        email: req.session.email,
        likes: (req.session.likedProperties && Object.keys(req.session.likedProperties).length > 0) ? true : false,
        propertyIds: (req.session.likedProperties) ? Object.keys(req.session.likedProperties) : [ ]
    }
    
    res.render('mobile/home', vm);
});

router.get('/bpower', function(req, res, next) {
  res.render('mobile/bpower', { title: 'Borrowing Power - Get Homii' });
});

router.get('/profile', function(req, res, next) {
  res.render('mobile/profile', { title: 'Profile - Get Homii' });
});

router.get('/search', function(req, res, next) {
    var vm = {
        title: 'Search - Get Homii',
        authenticated: (req.session.email) ? true : false,
        email: req.session.email
    }
    if (req.session.email) {
        userOrchestration.getCustomer(req.session.email)
            .then(function(customer) {
                if (customer.profileData && customer.profileData.search) {
                    vm.rent = customer.profileData.search.rent;
                    vm.frequency = customer.profileData.search.frequency;
                    vm.postcode = customer.profileData.search.postcode;
                    vm.likedProperties = customer.profileData.likedProperties;
                }
                res.render('mobile/search', vm);
            })
    } else {
        res.render('mobile/search', vm);
    }
});

router.post('/search', function(req, res, next) {
    var vm = {
        title: 'Search - Get Homii',
        rent: req.body.rent,
        frequency: req.body.frequency,
        postcode: req.body.postcode,
        authenticated: (req.session.email) ? true : false,
        email: req.session.email
    }
    res.render('mobile/results', vm);
});

router.get('/ajax/search', function(req, res, next) {

    var rent = req.query.rent;
    var frequency = req.query.frequency;
    var postcode = req.query.postcode;

    searchOrchestration.properties(rent, frequency, postcode, req.session.likedProperties)
        .then(function(results) {
            res.json(results);
        });
});

router.get('/ajax/comps', function(req, res, next) {
    if (!req.session.email) {
        return res.status(401);
    }

    var propertyId = req.query.propertyId;
    if (!propertyId) {
        return res.status(400);
    }

    searchOrchestration.comparableProperties(propertyId)
        .then(function(results) {
            return res.json(results);
        });
});

router.post('/ajax/like', function(req, res, next) {
    if (!req.session.email) {
        return res.status(401);
    }
    var email = req.session.email;
    var propertyId  = req.query.propertyId;
    console.log('email, propertyId', email, propertyId);
    userOrchestration.likeProperty(email, propertyId)
        .then(function(likedProperties) {
            console.log('likedProperties', likedProperties);
            req.session.likedProperties = likedProperties;
            return res.status(200);
        })
        .catch(function(error) {
            console.log('** ', error);
            return res.status(500);
        });
});

router.get('/register', function(req, res, next) {
    var vm = {
        title: 'Register - Get Homii',
        propertyId: req.query.propertyId,
        rent: req.query.rent,
        frequency: req.query.frequency,
        postcode: req.query.postcode
    }

    res.render('mobile/register', vm);
});

router.post('/register', function(req, res, next) {
    var vm = {
        title: 'Register - Get Homii'
    }

    var email = req.body.email;
    var password = req.body.password;
    var confirmPassword = req.body.confirmpassword;
    var propertyId = req.body.propertyId;
    var rent = req.body.rent;
    var frequency = req.body.frequency;
    var postcode = req.body.postcode;

    if (!email || email === '') {
        vm.errorMessage = "Email is required";
    } else if (!password || password === '') {
        vm.errorMessage = "Password is required";
    } else if (password != confirmPassword) {
        vm.errorMessage = "Passwords don't match";
    }
    
    if (!vm.errorMessage) {
        var profileData = {
            search: {
                rent,
                frequency,
                postcode
            },
            likedProperties: { }
        }
        if (propertyId) {
            profileData.likedProperties[propertyId] = 'Liked';
        }

        userOrchestration.createProfile(email, password, profileData)
            .then (function(result) {
                    vm.successMessage = "Successfully registered user"
                    vm.search = {
                        rent,
                        frequency,
                        postcode
                    }
                    vm.authenticated = true;
                    req.session.email = email;
                    req.session.likedProperties = profileData.likedProperties;
                    res.render('mobile/register', vm);
                }
            );        
    } else {
        res.render('mobile/register', vm);
    }
    
});

router.get('/login', function(req, res, next) {
    res.render('mobile/login', { title: 'Login - Get Homii' });
});

router.post('/login', function(req, res, next) {
    var vm = {
        title: 'Register - Get Homii'
    }

    var email = req.body.email;
    var password = req.body.password;
    
    if (!email || email === '') {
        vm.errorMessage = "Email is required";
    } else if (!password || password === '') {
        vm.errorMessage = "Password is required";
    } 
    
    if (!vm.errorMessage) {
        userOrchestration.authenticateUser(email, password) 
            .then(function(authenticatedUser) {
                if (!authenticatedUser.authenticated) {
                    vm.errorMessage = authenticatedUser.reason;
                    return res.render('mobile/login', vm);
                }
                req.session.email = email;
                req.session.likedProperties = authenticatedUser.profileData.likedProperties;

                vm.email = authenticatedUser.email;

                res.render('mobile/home', vm);
            })
    } else {
        res.render('mobile/home', vm);
    }
});

// GET for logout logout
router.get('/logout', function (req, res, next) {
    if (req.session) {
      // delete session object
      req.session.destroy(function (err) {
        if (err) {
          return next(err);
        } else {
          return res.redirect('/mobile/');
        }
      });
    }
});

module.exports = router;
